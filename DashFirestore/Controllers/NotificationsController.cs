﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DashFirestore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;

namespace DashFirestore.Controllers
{
    public class NotificationsController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.school_name = HttpContext.Session.GetString("school_name");
            return View("All");
        }

        public IActionResult SendtoAll([FromBody] AllNotificationModel model)
        {
            var client = new MongoClient(Constants.DB_URL);
            var database = client.GetDatabase("classociate");
            model.school_id = HttpContext.Session.GetString("school_id");
            var allnotifications = database.GetCollection<AllNotificationModel>("allstudentnotifications");
            allnotifications.InsertOne(model);
            var dictionary = new Dictionary<string, string>();
            dictionary["message"] = "Notification sent Successfully to all Students";
            return Json(dictionary);
        }
        
    }
}