﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DashFirestore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace DashFirestore.Controllers
{
    public class TimeTableController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.school_name = HttpContext.Session.GetString("school_name");
            return View("Timetable");
        }

        public IActionResult UploadTimeTable([FromBody] TimeTableModel model)
        {
            var dictionary = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(model.timetablejson);
            var client = new MongoClient(Constants.DB_URL);
            var database = client.GetDatabase("classociate");
            TimeTableMongoModel timetableModel = new TimeTableMongoModel();
            timetableModel.standard = model.standard;
            timetableModel.division = model.division;
            timetableModel.timetable = dictionary;
            timetableModel.school_id = HttpContext.Session.GetString("school_id");
            var timetablecollection = database.GetCollection<TimeTableMongoModel>("timetable");
            timetablecollection.InsertOne(timetableModel);
            return Json(dictionary);
        }


        public IActionResult GetStandardsDivisions()
        {
            var client = new MongoClient(Constants.DB_URL);
            var database = client.GetDatabase("classociate");
            var schoolfilterbuilder = Builders<BsonDocument>.Filter;
            var school_id = HttpContext.Session.GetString("school_id");
            var sfilter = schoolfilterbuilder.Eq("_id", new ObjectId(school_id));
            var school_doc = database.GetCollection<BsonDocument>("school").Find(sfilter).ToList();
            var dictionary = new Dictionary<string, object>();
            dictionary["standards"] = school_doc[0]["standards"];
            dictionary["divisions"] = school_doc[0]["divisions"];
            return Json(dictionary);
        }

        public IActionResult GetTimeTable([FromBody] GetTimeTableModel model)
        {
            var client = new MongoClient(Constants.DB_URL);
            var database = client.GetDatabase("classociate");
            var timetablefilterbuilder = Builders<BsonDocument>.Filter;
            model.school_id = HttpContext.Session.GetString("school_id");
            var filter = timetablefilterbuilder.And(timetablefilterbuilder.Eq("standard", model.standard),timetablefilterbuilder.Eq("division",model.division),timetablefilterbuilder.Eq("school_id",model.school_id));
            var timetable = database.GetCollection<BsonDocument>("timetable").Find(filter).ToList();
            var jsonstring = JsonConvert.SerializeObject(BsonSerializer.Deserialize<TimeTableMongoModel>(timetable[0]));
            var dictionary = new Dictionary<string, string>();
            dictionary["timetable"] = jsonstring;
            return Json(dictionary);


        }




    }
}