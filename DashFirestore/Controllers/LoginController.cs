﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DashFirestore.Models;
using Google.Cloud.Firestore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DashFirestore.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Index()
        {
            return View("Login");
        }


        public IActionResult Authenticate([FromBody]LoginModel model)
        {
            var client = new MongoClient(Constants.DB_URL);
            var database = client.GetDatabase("classociate");
            var collection = database.GetCollection<BsonDocument>("schoolusers");
            var builder = Builders<BsonDocument>.Filter;
            var filter = builder.Eq("user_id", model.loginUsername) & builder.Eq("password", model.loginPassword);
            var cursor = collection.Find(filter).ToList();


            if (cursor.Count > 0)
            {
                var schoolfilterbuilder = Builders<BsonDocument>.Filter;
                var school_id = cursor[0]["school_id"].ToString();
                var sfilter = schoolfilterbuilder.Eq("_id", new ObjectId(school_id));
                var school_doc = database.GetCollection<BsonDocument>("school").Find(sfilter).ToList();

                HttpContext.Session.SetString("school_id", school_id);
                //HttpContext.Session.SetString("name", school_doc[0]["name"].ToString());
                HttpContext.Session.SetString("school_name", school_doc[0]["name"].ToString());


                var dictionary = new Dictionary<string, string>();
                dictionary["message"] = "Success";
                return Json(dictionary);

               


            }
            else
            {
                var dictionary = new Dictionary<string, string>();
                dictionary["message"] = "Invalid";
                return Json(dictionary);
            }

            
        }
    }
}