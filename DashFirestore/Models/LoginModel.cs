﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashFirestore.Models
{
    public class LoginModel
    {
        public string loginUsername { get; set; }
        public string loginPassword { get; set; }
    }
}
