﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashFirestore.Models
{
    public class GetTimeTableModel
    {
        public string standard { get; set; }
        public string division { get; set; }
        public string school_id { get; set; }
    }
}
