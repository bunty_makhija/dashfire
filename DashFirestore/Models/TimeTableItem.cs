﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashFirestore.Models
{
    public class TimeTableItem
    {
        public string Day { get; set; }
        public string Subject { get; set; }
        public string Time { get; set; }
    }
}
