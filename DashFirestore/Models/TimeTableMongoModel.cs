﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashFirestore.Models
{
    public class TimeTableMongoModel
    {
        public ObjectId _id { get; set; }
        public string school_id { get; set; }
        public string standard { get; set; }
        public string division { get; set; }
        public List<Dictionary<string,string>> timetable { get; set; }
    }
}
