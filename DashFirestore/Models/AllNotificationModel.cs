﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashFirestore.Models
{
    public class AllNotificationModel
    {
        public ObjectId _id { get; set; }
        public string title { get; set; }
        public string body { get; set; }

        public string school_id { get; set; }
    }
}
