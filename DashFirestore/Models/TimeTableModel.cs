﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashFirestore.Models
{
    public class TimeTableModel
    {
        //public List<TimeTableItem> items { get; set; }
        public string timetablejson { get; set; }
        public string standard { get; set; }
        public string division { get; set; }

        public string school_id { get; set; }
    }
}
