﻿app.controller('allnotification', ['$scope', '$http', '$window', function ($scope, $http, $window) {

    $scope.sendallnotification = function () {
        $http.post('/Notifications/SendtoAll', {
            'title': $scope.notification_title,
            'body': $scope.notification_body
        }, {
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                //console.log(response.data);
                $scope.notification_title = null;
                $scope.notification_body = null;
                bootbox.alert(response.data.message);

            }, function (error) {
                console.log(error);
            });
    };

}]);