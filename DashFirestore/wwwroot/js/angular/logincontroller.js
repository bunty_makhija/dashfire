﻿app.controller('logincontroller', ['$scope', '$http', '$window', function ($scope, $http, $window) {

    $scope.showError = false
    $scope.loading = false
    $scope.login = function () {
        console.log($scope.username);
        console.log($scope.password);
        $scope.loading = true;
        $http.post('/Login/Authenticate', {
            'loginUsername': $scope.username,
            'loginPassword': $scope.password
        }, {
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                console.log(response.data);
                $scope.loading = false;
                var message = response.data.message;
                console.log(message);
                if (message == "Success") {
                    window.location.href = '/Home/Index';
                } else {
                    $scope.showError = true;
                }
               
            }, function (error) {
                console.log(error);
            });
    };


    $scope.continueShop = function () {
        window.location.href = '/Shop/Index';
    };

}]);