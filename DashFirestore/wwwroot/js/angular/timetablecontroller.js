﻿app.controller('timetablecontroller', ['$scope', '$http', '$window', function ($scope, $http, $window) {

    $scope.SelectFile = function (file) {
        $scope.SelectedFile = file;
    };

    getStandards();
    $scope.Upload = function () {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
        if (regex.test($scope.SelectedFile.name.toLowerCase())) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
                //For Browsers other than IE.
                if (reader.readAsBinaryString) {
                    reader.onload = function (e) {
                        $scope.ProcessExcel(e.target.result);
                    };
                    reader.readAsBinaryString($scope.SelectedFile);
                } else {
                    //For IE Browser.
                    reader.onload = function (e) {
                        var data = "";
                        var bytes = new Uint8Array(e.target.result);
                        for (var i = 0; i < bytes.byteLength; i++) {
                            data += String.fromCharCode(bytes[i]);
                        }
                        $scope.ProcessExcel(data);
                    };
                    reader.readAsArrayBuffer($scope.SelectedFile);
                }
            } else {
                $window.alert("This browser does not support HTML5.");
            }
        } else {
            $window.alert("Please upload a valid Excel file.");
        }
    };


    $scope.getTimeTable = function () {
        $http.post('/TimeTable/GetTimeTable', {
            'standard': $scope.selectedStandard,
            'division': $scope.selectedDivision
        }, {
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {

                var fetchtimetableobject = response.data.timetable;
                var json = JSON.parse(fetchtimetableobject);
                //console.log(json);
                //console.log(fetchtimetableobject[0]);
                $scope.fetch_timetable = json.timetable;
                $scope.fkeys = [];
                //$scope.keys.push(Object.keys());
                
                Object.keys(json.timetable[0]).forEach(function (key) {
                    $scope.fkeys.push(key);
                });
                
            }, function (error) {
                console.log(error);
            });
    };


    $scope.ProcessExcel = function (data) {
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        //Display the data from Excel file in Table.
        $scope.$apply(function () {
            $scope.Customers = excelRows;
            $scope.IsVisible = true;
            console.log('************');
            console.log($scope.Customers);

            $http.post('/TimeTable/UploadTimeTable', {
                'timetablejson': JSON.stringify($scope.Customers),
                'standard': $scope.selectedStandardUpload,
                'division': $scope.selectedDivisionUpload
            }, {
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    //console.log(response.data);
                    $scope.loading = false;
                    var timetableobject = response.data;
                    console.log(timetableobject);
                    $scope.timetable = timetableobject;
                    $scope.keys = [];
                    //$scope.keys.push(Object.keys());

                    Object.keys(timetableobject[0]).forEach(function (key) {
                        $scope.keys.push(key);
                    });

                    bootbox.alert("Timetabe uplaoded Successfully for " + $scope.selectedStandardUpload + " " + $scope.selectedDivisionUpload);

                }, function (error) {
                    console.log(error);
                });
           


        });
    };




    function getStandards() {
        $http.get('/TimeTable/GetStandardsDivisions')
            .then(function (response) {
                $scope.standards = response.data.standards;
                $scope.divisions = response.data.divisions;
            }, function (error) {
                console.log(error);
            });
    };

}]);